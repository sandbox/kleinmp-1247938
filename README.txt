$Id: README.txt,v 1.2.2.1.2.1.2.3 2011/01/25 13:49:22 ilo Exp $

SOAP Server
===========
<<<<<<< HEAD
The SOAP Server module allows a Drupal site to access services using the same
callback code.



Requirements to call services via SOAP Server
---------------------------------------------
 1) Enable clean URLs from Drupal site configrations
   
 2) Install the NuSOAP library so that it ends up like the following:
     soap_server/nusoap/
     soap_server/nusoap/nusoap.txt
     soap_server/nusoap/lib
     soap_server/nusoap/lib/nusoap.php
     
 3) Allow anonymous user to access services module from access control
 
 4) Remove key or session requeriments from service configuration


How to create client to soap server
-----------------------------------
This is a simple client to call node.load from node services and user.login.


soap_server_test.php
<?php
  // Pull in the NuSOAP code
  // Create the client instance
  // TODO Set the paht of wsdl
  $wsdl = "http://localhost/drupal/services/soap?wsdl";
  $client = new soapClient($wsdl);

  // Call the SOAP method
  // Set method parameters
  /*
   * node.load params are 
   *  1- nid => int
   *  2- fields => array (optional)
   *  Let we call methos twice with & without fields 
   *  then we have $param1 & $param2
   */
  //set $fields this is an optional arg
  $fields = array('nid', 'title', 'type');
  $param1 = array(
    'nid'=> 3, //Set nid
  );
  $param2 = array(
    'nid'=> 3, 
    'fields' => $fields
  );
  $param3 = array(
    'username'=> 'user', 
    'password' => 'password'
  );
=======
>>>>>>> Adding the first version of Soap Server for Services 3

Soap Server for Services 6.x-3.x

WARNING:  This module is a prototype and is not secure.  DO NOT USE FOR PRODUCTION SITES

GETTING STARTED
===============

Enable the module
Add an endpoint at admin/build/services/add choosing SOAP as the Server
Enable the node and user retrieve methods at admin/build/services/{endpoint_name}/resources
View the generated WSDL at soap_server/debug_wsdl/{endpoint_name}
Give anonymous user 'access soap server' permission  - NOT FOR PRODUCTION USE
Use SOAPUI or ther test client to import the WSDL from http://example.com/{endpoint_path}?wsdl
or
Enable devel module
View a node object and user object debug client at soap_server/debug_client/{endpoint_name}/{nid}
The debug client will show:
ARGS
Endpoint object
Node object

Available client functions
The XML of the request
The XML of the response
The node object retrieved from the node.retrieve service from the supplied nid
The user object for user 1

NOTES
+++++

If an endpoint is configured to use one or more Hose XML endpoints then Hose XML will take over 
WSDL generation for the endpoint which may have unexpected results for Services (or other) resources. 
